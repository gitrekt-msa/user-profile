package com.gitrekt.quora.useredit;

import com.gitrekt.quora.database.postgres.handlers.UserReportPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.UsersPostgresHandler;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.UUID;

public class UserReportTest {

    protected UserReportPostgresHandler userReportPostgresHandler;

    @Before
    public void init() {
        userReportPostgresHandler = new UserReportPostgresHandler();
    }


    @Test
    public void checkUserReport() throws SQLException {
        String userId = "0d50fcd3-eed6-4774-8ac0-41c0c2b72b0d";
        String reportedId = "0d50fcd3-eed6-4774-8ac0-42c0c2b72b0d";

        userReportPostgresHandler.reportUser(UUID.randomUUID(), userId, reportedId);

    }
}

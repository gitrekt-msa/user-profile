package com.gitrekt.quora.useredit;

import com.gitrekt.quora.database.postgres.handlers.UsersPostgresHandler;
import com.gitrekt.quora.models.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.sql.SQLException;
import java.util.HashMap;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class UserEditTest {
    protected UsersPostgresHandler usersPostgresHandler;

    @Before
    public void init() {
        usersPostgresHandler = new UsersPostgresHandler();
    }


    @Test
    public void checkUserEdit() throws SQLException {

        String uuid = "0d50fcd3-eed6-4774-8ac0-42c0c2b72b0d";
        String password = BCrypt.hashpw("strongpassword", BCrypt.gensalt(12));
        String email = "hello@tum.de";
        String username = "tum-zarei";
        String fname = "mohamed";
        String lname = "tum";

        usersPostgresHandler.editUser(uuid, email, username, password, fname, lname);

        User fetchedUser = usersPostgresHandler.getUser(uuid);
        System.out.println(fetchedUser);
        assertEquals(email, fetchedUser.getEmail());
        assertEquals(username, fetchedUser.getUserName());
        assertEquals(fname, fetchedUser.getFirstName());
        assertEquals(lname, fetchedUser.getLastName());
    }
}

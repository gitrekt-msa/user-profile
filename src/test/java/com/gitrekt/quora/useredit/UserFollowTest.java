package com.gitrekt.quora.useredit;

import com.gitrekt.quora.database.postgres.handlers.FollowersPostgresHandler;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;


public class UserFollowTest {

    protected FollowersPostgresHandler followersPostgresHandler;

    @Before
    public void init() {
        followersPostgresHandler = new FollowersPostgresHandler();
    }


    @Test
    public void checkUserFollow() throws SQLException {
        String userId = "0d50fcd3-eed6-4774-8ac0-41c0c2b72b0d";
        String followedId = "0d50fcd3-eed6-4774-8ac0-42c0c2b72b0d";

        try{
            followersPostgresHandler.followUser(userId, followedId);
        }catch (SQLException ex){
            followersPostgresHandler.unfollowUser(userId, followedId);
            followersPostgresHandler.followUser(userId, followedId);
            followersPostgresHandler.unfollowUser(userId, followedId);
        }

    }
}

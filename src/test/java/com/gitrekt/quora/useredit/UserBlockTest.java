package com.gitrekt.quora.useredit;

import com.gitrekt.quora.database.postgres.handlers.BlockPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.FollowersPostgresHandler;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

public class UserBlockTest {

    protected BlockPostgresHandler blockPostgresHandler;

    @Before
    public void init() {
        blockPostgresHandler = new BlockPostgresHandler();
    }


    @Test
    public void checkUserBlock() throws SQLException {
        String userId = "0d50fcd3-eed6-4774-8ac0-41c0c2b72b0d";
        String blockedId = "7b33e4fc-1a41-4661-a4d9-563fc21cd89e";

        try{
            blockPostgresHandler.blockUser(userId, blockedId);
        }catch (Exception ex){
            blockPostgresHandler.unblockUser(userId, blockedId);
            blockPostgresHandler.blockUser(userId, blockedId);
            blockPostgresHandler.unblockUser(userId, blockedId);
        }
    }

}

package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.models.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class BlockPostgresHandler  extends PostgresHandler<User> {

    public BlockPostgresHandler() {
        super("user_block", User.class);
    }


    /**
     * Insert a block instance in the database by calling the `Insert_user_block` procedure.
     *
     * @param params The parameters to the procedure
     * @throws SQLException if an error occurred when inserting into the database
     */
    public void blockUser(Object... params) throws SQLException {
        int[] types = new int[params.length];
        for (int i = 0; i < params.length; i++) {
            types[i] = Types.OTHER;
        }
        try (Connection connection = PostgresConnection.getInstance().getConnection()) {
            super.call("CALL Insert_user_block(?, ?)", connection, types, params);
        }
    }

    /**
     * Soft Delete a block instance in the database by calling the `Delete_user_block` procedure.
     *
     * @param params The parameters to the procedure
     * @throws SQLException if an error occurred when inserting into the database
     */
    public void unblockUser(Object... params) throws SQLException {
        int[] types = new int[params.length];
        for (int i = 0; i < params.length; i++) {
            types[i] = Types.OTHER;
        }
        try (Connection connection = PostgresConnection.getInstance().getConnection()) {
            super.call("CALL Delete_user_block(?, ?)",connection, types, params);
        }
    }


    public boolean isBlocked(Object... params) throws SQLException{

        int[] types = new int[params.length];
        for (int i = 0; i < params.length; i++) {
            types[i] = Types.OTHER;
        }
        try (Connection connection = PostgresConnection.getInstance().getConnection()) {
            ResultSet resultSet = super.call("SELECT * FROM Get_Blocked_State(?, ?)",connection, types, params);

            if (resultSet.next()) {
                String deleted_at = resultSet.getString("deleted_at");
                if (deleted_at != null)
                    return false;
                else
                    return true;
            }
        }
            return false;
    }




}

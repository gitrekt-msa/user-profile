package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.models.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class UserReportPostgresHandler extends PostgresHandler<User> {

    public UserReportPostgresHandler() {
        super("user_reports", User.class);
    }


    /**
     * Insert a report instance in the database by calling the `Insert_user_report` procedure.
     *
     * @param params The parameters to the procedure
     * @throws SQLException if an error occurred when inserting into the database
     */
    public void reportUser(Object... params) throws SQLException {
        int[] types = new int[params.length];
        for (int i = 0; i < params.length; i++) {
            types[i] = Types.OTHER;
        }
        try (Connection connection = PostgresConnection.getInstance().getConnection()) {
            super.call("CALL Insert_user_report(?, ?, ?)",connection, types, params);
        }
    }


}

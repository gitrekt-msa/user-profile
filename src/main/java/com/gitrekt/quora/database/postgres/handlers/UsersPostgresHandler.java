package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.models.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class UsersPostgresHandler extends PostgresHandler<User> {

  public UsersPostgresHandler() {
    super("Users", User.class);
  }

  /**
   * Edit a user in the database by calling the `Edit_User` procedure.
   *
   * @param params The parameters to the procedure
   * @throws SQLException if an error occurred when inserting into the database
   */
  public void editUser(Object... params) throws SQLException {
    int[] types = new int[params.length];
    types[0] = Types.OTHER;
    for (int i = 1; i < params.length; i++) {
      types[i] = Types.VARCHAR;
    }
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      super.call("CALL Edit_User(?, ?, ?, ?, ?, ?)",connection, types, params);
    }
  }


  public User getUser(Object... params) throws SQLException {
    int[] types = new int[params.length];
    types[0] = Types.OTHER;
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {

      ResultSet resultSet = super.call("SELECT * FROM Get_User_Profile_Info(?)",connection, types, params);

      if (resultSet == null || !resultSet.next()) {
        return null;
      }


      User user = new User();

      user.setUserId(resultSet.getString("id"));
      user.setFirstName(resultSet.getString("first_name"));
      user.setLastName(resultSet.getString("last_name"));
      user.setUserName(resultSet.getString("username"));
      user.setEmail(resultSet.getString("email"));
      return user;

    }
  }

}

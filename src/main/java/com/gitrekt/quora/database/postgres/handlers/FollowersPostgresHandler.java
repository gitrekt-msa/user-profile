package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.models.User;

import java.sql.*;

public class FollowersPostgresHandler extends PostgresHandler<User> {

    public FollowersPostgresHandler() {
        super("Followers", User.class);
    }

    /**
     * Insert a follow instance in the database by calling the `Insert_follower` procedure.
     *
     * @param params The parameters to the procedure
     * @throws SQLException if an error occurred when inserting into the database
     */
    public void followUser(Object... params) throws SQLException {
        int[] types = new int[params.length];
        for (int i = 0; i < params.length; i++) {
            types[i] = Types.OTHER;
        }
        try (Connection connection = PostgresConnection.getInstance().getConnection()) {
            super.call("CALL Insert_follower(?, ?)", connection,types, params);
        }
    }


    /**
     * Delete a follow instance from the database by calling the `Delete_follower` procedure.
     *
     * @param params The parameters to the procedure
     * @throws SQLException if an error occurred when inserting into the database
     */
    public void unfollowUser(Object... params) throws SQLException {
        int[] types = new int[params.length];
        for (int i = 0; i < params.length; i++) {
            types[i] = Types.OTHER;
        }
        try (Connection connection = PostgresConnection.getInstance().getConnection()) {
            super.call("CALL Delete_follower(?, ?)",connection, types, params);
        }
    }

    public boolean isFollowed(Object... params) throws SQLException{

        int[] types = new int[params.length];
        for (int i = 0; i < params.length; i++) {
            types[i] = Types.OTHER;
        }
        try (Connection connection = PostgresConnection.getInstance().getConnection()) {
            ResultSet resultSet = super.call("SELECT * FROM Get_Follow_State(?, ?)",connection, types, params);
            if (resultSet.next()) {
                String uId = resultSet.getString("user_id");
                if (uId != null)
                    return true;
                else
                    return false;
            }
            return false;
        }
    }
}

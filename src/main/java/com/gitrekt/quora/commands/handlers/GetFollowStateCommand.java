package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.BlockPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.FollowersPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;

import java.sql.SQLException;
import java.util.HashMap;

public class GetFollowStateCommand extends Command {
    private static final String[] argumentNames =
            new String[] {"userId", "followedId"};

    public GetFollowStateCommand(HashMap<String, Object> args) {
        super(args);
        setPostgresHandler(new FollowersPostgresHandler());
    }

    @Override
    public Object execute() throws SQLException, BadRequestException, AuthenticationException {
        checkArguments(argumentNames);

        FollowersPostgresHandler handler = (FollowersPostgresHandler)postgresHandler;
        String userId =  (String) args.get("userId");
        String followedId =  (String) args.get("followedId");
        Boolean followState = handler.isFollowed(userId, followedId);
        return followState;
    }
}

package com.gitrekt.quora.commands.handlers;

        import com.gitrekt.quora.commands.Command;
        import com.gitrekt.quora.database.postgres.handlers.FollowersPostgresHandler;
        import com.gitrekt.quora.database.postgres.handlers.UserReportPostgresHandler;
        import com.gitrekt.quora.database.postgres.handlers.UsersPostgresHandler;
        import com.gitrekt.quora.exceptions.AuthenticationException;
        import com.gitrekt.quora.exceptions.BadRequestException;
        import com.gitrekt.quora.models.User;
        import com.google.gson.Gson;

        import java.sql.SQLException;
        import java.util.HashMap;

public class GetUserInfoCommand extends Command {

    private static final String[] argumentNames =
            new String[] {"userId"};

    public GetUserInfoCommand(HashMap<String, Object> args) {
        super(args);
        setPostgresHandler(new UsersPostgresHandler());
    }

    @Override
    public Object execute() throws SQLException, BadRequestException, AuthenticationException {

//        System.out.println("hady");
        checkArguments(argumentNames);
        String userId = (String) args.get("userId");

        UsersPostgresHandler handler = (UsersPostgresHandler)postgresHandler;
        User user = handler.getUser(userId);
        Gson gson = new Gson();
        return gson.toJson(user);
    }
}

package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.FollowersPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;

import java.sql.SQLException;
import java.util.HashMap;
import com.rabbitmq.client.Channel;
import com.gitrekt.quora.queue.MessageQueueConnection;
import com.google.gson.JsonObject;

public class FollowUserCommand extends Command{

    private static final String[] argumentNames =
            new String[] {"followedId", "userId"};

    public FollowUserCommand(HashMap<String, Object> args) {
        super(args);
        setPostgresHandler(new FollowersPostgresHandler());
    }

    @Override
    public Object execute() throws SQLException, BadRequestException, AuthenticationException {
        checkArguments(argumentNames);

        String followedId = (String) args.get("followedId");
        String userId = (String) args.get("userId");

        FollowersPostgresHandler handler = (FollowersPostgresHandler)postgresHandler;

        handler.followUser(userId, followedId);



        try {
            Channel channel = MessageQueueConnection.getInstance().createChannel();
            JsonObject msg = new JsonObject();
            msg.addProperty("senderId",userId);
            msg.addProperty("type", "new_follower");

            channel.basicPublish("", "notification-v1-queue", null, msg.toString().getBytes());
            channel.close();

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return "User followed successfully!";
    }

}

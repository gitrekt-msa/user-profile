package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.FollowersPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.UserReportPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class ReportUserCommand extends Command {

    private static final String[] argumentNames =
            new String[] {"userId", "reportedId"};

    public ReportUserCommand(HashMap<String, Object> args) {
        super(args);
        setPostgresHandler(new UserReportPostgresHandler());
    }

    @Override
    public Object execute() throws SQLException, BadRequestException, AuthenticationException {
        checkArguments(argumentNames);

        String userId = (String) args.get("userId");
        String reportedId = (String) args.get("reportedId");


        UserReportPostgresHandler handler = (UserReportPostgresHandler)postgresHandler;
        handler.reportUser(UUID.randomUUID(), userId, reportedId);

        return "User reported successfully!";
    }
}

package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.BlockPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.UsersPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.User;

import java.sql.SQLException;
import java.util.HashMap;

public class GetUserBlockStateCommand extends Command {

    private static final String[] argumentNames =
            new String[] {"userId", "blockedId"};

    public GetUserBlockStateCommand(HashMap<String, Object> args) {
        super(args);
        setPostgresHandler(new BlockPostgresHandler());
    }

    @Override
    public Object execute() throws SQLException, BadRequestException, AuthenticationException {
        checkArguments(argumentNames);

        BlockPostgresHandler handler = (BlockPostgresHandler)postgresHandler;
        String userId =  (String) args.get("userId");
        String blockedId =  (String) args.get("blockedId");
        Boolean blockState = handler.isBlocked(userId, blockedId);
        return blockState;
    }
}

package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.FollowersPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.UsersPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class EditUserCommand extends Command {

    private static final String[] argumentNames =
            new String[] {"username", "email", "password", "confirmPassword", "firstName", "lastName", "userId"};

    public EditUserCommand(HashMap<String, Object> args) {
        super(args);
        setPostgresHandler(new UsersPostgresHandler());
    }
    @Override
    public Object execute() throws SQLException, BadRequestException, AuthenticationException {

        checkArguments(argumentNames);

        String email = (String) args.get("email");
        String password = (String) args.get("password");
        String confirmPassword = (String) args.get("confirmPassword");

        validate(email, password, confirmPassword);

        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt(12));
        String username = (String) args.get("username");
        String firstName = (String) args.get("firstName");
        String lastName = (String) args.get("lastName");

        String userId = (String) args.get("userId");

        UsersPostgresHandler handler = (UsersPostgresHandler)postgresHandler;
        handler.editUser(
                userId, email, username, hashedPassword, firstName, lastName);

        return "Edit Successful !";
    }

    private void validate(String email, String password, String confirmPassword)
            throws BadRequestException {
        StringBuilder stringBuilder = new StringBuilder();
        if (!EmailValidator.getInstance().isValid(email)) {
            stringBuilder.append("Invalid Email").append("\n");
        }
        if (!password.equals(confirmPassword)) {
            stringBuilder.append("'Password' and 'Confirm Password' do not match").append("\n");
        }
        if (stringBuilder.length() > 0) {
            throw new BadRequestException(stringBuilder.toString());
        }
    }
}

package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.BlockPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.FollowersPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.UsersPostgresHandler;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.exceptions.BadRequestException;

import java.sql.SQLException;
import java.util.HashMap;

public class BlockUserCommand extends Command {

    private static final String[] argumentNames =
            new String[] {"blockedId", "userId"};


    public BlockUserCommand(HashMap<String, Object> args) {
        super(args);
        setPostgresHandler(new BlockPostgresHandler());
    }

    @Override
    public Object execute() throws SQLException, BadRequestException, AuthenticationException {
        checkArguments(argumentNames);

        String userId = (String) args.get("userId");
        String blockedId = (String) args.get("blockedId");


        BlockPostgresHandler handler = (BlockPostgresHandler)postgresHandler;
        handler.blockUser(userId, blockedId);


        return "User blocked successfully!";
    }
}
